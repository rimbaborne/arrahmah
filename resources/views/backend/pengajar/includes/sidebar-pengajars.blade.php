<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/pengajars*')) }}" href="{{ route('admin.pengajars.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_pengajars.sidebar.title')
    </a>
</li>