<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/absens*')) }}" href="{{ route('admin.absens.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_absens.sidebar.title')
    </a>
</li>