<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/tahsins*')) }}" href="{{ route('admin.tahsins.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_tahsins.sidebar.title')
    </a>
</li>