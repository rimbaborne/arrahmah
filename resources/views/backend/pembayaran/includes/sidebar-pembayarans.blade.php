<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/pembayarans*')) }}" href="{{ route('admin.pembayarans.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_pembayarans.sidebar.title')
    </a>
</li>