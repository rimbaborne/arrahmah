<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/amalans*')) }}" href="{{ route('admin.amalans.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_amalans.sidebar.title')
    </a>
</li>