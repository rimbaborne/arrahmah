<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/jadwals*')) }}" href="{{ route('admin.jadwals.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_jadwals.sidebar.title')
    </a>
</li>