<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/rtqs*')) }}" href="{{ route('admin.rtqs.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_rtqs.sidebar.title')
    </a>
</li>